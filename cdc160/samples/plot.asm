; simple plotter test
        .ORIGIN 00010
dist:   .WORD   00000
cmd:    .WORD   00000
        .START
        EXC     04401
        OTN     00020   ; pen down
        LDN     00001   ; right
        STD     cmd
        LDN     00024
        STD     dist
        JPR     repeat  ; 10 times move right
        OTN     00040   ; pen up
        LDN     00012   ; left-up
        STD     cmd
        LDN     00012
        STD     dist
        JPR     repeat  ; 10 times move left-up
        OTN     00020   ; pen down
        LDN     00004   ; down
        STD     cmd
        LDN     00024
        STD     dist
        JPR     repeat  ; 10 times move down
        HLT
; send dist times command from cmd
repeat: .WORD   00000
repe1:  LDD     cmd
        OTA
        LDD     dist
        SBN     00001
        STD     dist
        NZB     repe1
        JPI     repeat
