;
; Copyright (c) 2004 Michal Brzozowski
;
; simple benchmark for CDC-160A
;
; for (cnt2 = 2000; cnt2 != 0; --cnt2) {
;     for (cnt = 2000; cnt != 0; --cnt) {
;         tmp   += 03333;
;     }
; }
;
; Altogether test has 2001*2000*12 = 48024000 machine cycles, which
; with 6.4us cycle in the original machine should take ca 307.35s 
; Emulator built with gcc 3.3.2 with option -O2 on Linux running on
; Athlon 1.2GHz 512MB DDR RAM makes it in 5.5s

        .WORD   00000
cnt:    .WORD   03720
cnt2:   .WORD   03720
tmp:    .WORD   00000
        .START
loop:   LDC     03333   ; 2
        RAD     tmp     ; 3
        LDD     cnt     ; 2
        SBN     001     ; 1
        STD     cnt     ; 3
        NZB     loop    ; 1
                        ; = 12 * 2000 * 2000
        LDC     03720   ; 2
        STD     cnt     ; 3
        LDD     cnt2    ; 2
        SBN     001     ; 1
        STD     cnt2    ; 3
        NZB     loop    ; 1
        HLT             ; = 12 * 2000
