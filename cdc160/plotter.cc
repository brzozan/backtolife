/*
 * plotter.cc	$Revision: 1.4 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "plotter.h"

namespace cdc160
{

Plotter::Plotter() : m_active(false), m_penDown(false), m_headerSent(false), m_streamOut(&std::cout),
    m_lastDx(0), m_lastDy(0), m_totalDx(0), m_totalDy(0), m_haveStored(false)
{
}

void Plotter::setStreamOut(std::ostream *strOut)
{
    if (strOut != NULL) {
        m_streamOut = strOut;
    } else {
        m_streamOut = &std::cout;
    }
}

void Plotter::flushMove(const char *operation)
{
    // convert offsets from plotter units (1/100") to PostScript units (1/72")
    *m_streamOut << (0.72 * m_totalDx) << " " << (0.72 * m_totalDy) << " " << operation << std::endl;
}

void Plotter::flush()
{
    if (m_headerSent) {
        if (m_haveStored) {
            if (m_penDown) {
                flushMove("rlineto");
            }
        }
        *m_streamOut << "closepath" << std::endl;
        *m_streamOut << "stroke" << std::endl;
        *m_streamOut << "showpage" << std::endl;
        m_headerSent    = false;
    }
}

void Plotter::command(unsigned short cmd)
{
    // currently write-mode only
    if (cmd == 04401) {
        m_active    = true;
        
        if (!m_headerSent) {
            *m_streamOut << "%!" << std::endl;
            *m_streamOut << "newpath" << std::endl;
            *m_streamOut << "100 100 moveto" << std::endl;
            m_headerSent    = true;
        }
    } else {
        m_active    = false;
    }
}

bool Plotter::active()
{
    return m_active;
}

void Plotter::output(unsigned short data)
{
    if (m_active) {
        bool    error   = false;
        int     dx      = 0;
        int     dy      = 0;
        
        if (data == 00020) {
            if (!m_penDown) {
                if (m_haveStored) {
                    flushMove("rmoveto");
                    m_totalDx       = 0;
                    m_totalDy       = 0;
                    m_haveStored    = false;
                }
                m_penDown   = true;
            }
        } else if (data == 00040) {
            if (m_penDown) {
                if (m_haveStored) {
                    flushMove("rlineto");
                    m_totalDx       = 0;
                    m_totalDy       = 0;
                    m_haveStored    = false;
                }
                m_penDown   = false;
            }
        } else {
            int xcmd    = data & 00003;
            int ycmd    = data & 00014;

            if (xcmd == 00000) {
                // pass
            } else if (xcmd == 00001) {
                dx  = 1;
            } else if (xcmd == 00002) {
                dx  = -1;
            } else {
                error   = true;
            }
            
            if (ycmd == 00000) {
                // pass
            } else if (ycmd == 00004) {
                dy  = 1;    // rotate drum in -Y direction -> dy = 1
            } else if (ycmd == 00010) {
                dy  = -1;   // rotate drum in +Y direction -> dy = -1
            } else {
                error   = true;
            }

            if (!error) {
                // accumulate offset if either:
                // - no offset has been stored so far
                // - pen is up (in which case you can freely move around)
                // - offset is the same as in previous operation
                if (!m_haveStored || !m_penDown || (dx == m_lastDx && dy == m_lastDy)) {
                    m_totalDx       += dx;
                    m_totalDy       += dy;
                } else {
                    // otherwise draw line with accumulated offset
                    // (entering here implies m_penDown)
                    if (m_penDown) {
                        flushMove("rlineto");
                    }
                    m_totalDx       = dx;
                    m_totalDy       = dy;
                }
                m_lastDx        = dx;
                m_lastDy        = dy;
                m_haveStored    = true;
            }
        }
    }
}

unsigned short Plotter::input()
{
    return 00000;
}

unsigned short Plotter::status()
{
    // report ready
    return 00000;
}


}

