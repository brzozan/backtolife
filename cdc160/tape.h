/*
 * tape.h	$Revision: 1.4 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __tape_h__
#define __tape_h__

#include <iostream>

#ifndef __device_h__
#include "device.h"
#endif

namespace cdc160
{
    /**
     * 350 paper tape reader. Selected by command 04102.
     * FIXME dummy implementation - no input possible.
     */
    class TapeReader : public Device
    {
        public:
            TapeReader();
            
            void    setStream(std::istream *str);
            
            virtual void command(unsigned short cmd);
            virtual bool active();
            virtual void output(unsigned short data);
            virtual unsigned short input();
            virtual unsigned short status();

        private:
            bool            m_active;
            std::istream    *m_stream;
    };

    /**
     * BRPE-11 paper tape punch. Selected by command 04104.
     * Tape level (number of stored bits per word) is set during object
     * creation.
     * Output is directed to standard output.
     */
    class TapePuncher : public Device
    {
        public:
            TapePuncher();
            
            void    setStream(std::ostream *str);
            void    setChannels(int channels);
            
            virtual void command(unsigned short cmd);
            virtual bool active();
            virtual void output(unsigned short data);
            virtual unsigned short input();
            virtual unsigned short status();

        private:
            bool            m_active;
            int             m_channels;
            std::ostream    *m_stream;
    };
}


#endif /* __tape_h__ */

