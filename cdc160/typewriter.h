/*
 * typewriter.h	$Revision: 1.4 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __typewriter_h__
#define __typewriter_h__

#include <iostream>

#ifndef __device_h__
#include "device.h"
#endif

namespace cdc160
{

    /**
     * Input/Output Typewriter emulation.
     */
    class Typewriter : public Device
    {
        public:
            /**
             * Constructs Typewriter instance configured as primary
             * (primaryDevice == true) or secondary typewriter.
             * Primary typewriter is selected by commands 042xx, while
             * secondary is selected by 043xx.
             */
            Typewriter(bool primaryDevice);
            
            void    setStreamIn(std::istream *strIn);
            void    setStreamOut(std::ostream *strOut);
            
            virtual void command(unsigned short cmd);
            virtual bool active();
            virtual void output(unsigned short data);
            virtual unsigned short input();
            virtual unsigned short status();

        private:
            char    translate(char chr) const;
            char    untranslate(char chr) const;

            bool            m_active;
            bool            m_inputMode;
            bool            m_upperCase;
            bool            m_primaryDevice;
            std::istream    *m_streamIn;
            std::ostream    *m_streamOut;
    };
    
}

#endif /* __typewriter_h__ */

