/*
 * tape.cc	$Revision: 1.6 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include "tape.h"

namespace cdc160
{

//----------------------------------------------------------------------------
// TapeReader
//----------------------------------------------------------------------------

TapeReader::TapeReader() : m_active(false), m_stream(&std::cin)
{
}

void TapeReader::command(unsigned short cmd)
{
    if (cmd == 04102) {
        m_active    = true;
    } else {
        m_active    = false;
    }
}

bool TapeReader::active()
{
    return m_active;
}

void TapeReader::output(unsigned short data)
{
}

unsigned short TapeReader::input()
{
    unsigned short  result  = 0;
    bool            inTape  = false;
    
    if (m_active) {
        // read single tape row
        // FIXME this actually ignores number of channels...
        while (!m_stream->eof()) {
            char    chr = m_stream->get();

            if (!inTape) {
                // not in tape - skip characters until | is encountered
                if (chr == '|') {
                    inTape  = true;
                }
            } else {
                // inside tape:
                // - blank character means 0 bit
                // - . is ignored
                // - | ends tape
                // - other characters - 1 bit
                
                if (std::isspace(chr)) {
                    // bit 0 - just shift previous bits one position left
                    result  <<= 1;
                } else if (chr == '.') {
                    // ignore
                } else if (chr == '|') {
                    // end of tape row
                    break;
                } else {
                    // non-blank - bit 1
                    result  <<= 1;
                    result  |= 1;
                }
            }
        }
    }
    return result;
}

unsigned short TapeReader::status()
{
    return 0;
}

void TapeReader::setStream(std::istream *str)
{
    if (str != NULL) {
        m_stream    = str;
    } else {
        m_stream    = &std::cin;
    }
}

//----------------------------------------------------------------------------
// TapeReader
//----------------------------------------------------------------------------

TapePuncher::TapePuncher() : m_active(false), m_channels(7), m_stream(&std::cout)
{
}

void TapePuncher::setStream(std::ostream *str)
{
    if (str != NULL) {
        m_stream    = str;
    } else {
        m_stream    = &std::cout;
    }
}

void TapePuncher::setChannels(int channels)
{
    m_channels  = channels;
}

void TapePuncher::command(unsigned short cmd)
{
    if (cmd == 04104) {
        m_active    = true;
    } else {
        m_active    = false;
    }
}

bool TapePuncher::active()
{
    return m_active;
}

void TapePuncher::output(unsigned short data)
{
    *m_stream << '|';
    for (int i = m_channels - 1; i >= 0; --i) {
        if (i == 2) {
            *m_stream << '.';
        }
        if ((data & (1u << i)) != 0) {
            *m_stream << 'o';
        } else {
            *m_stream << ' ';
        }
    }
    *m_stream << '|' << std::endl;
}

unsigned short TapePuncher::input()
{
    return 0;
}

unsigned short TapePuncher::status()
{
    return 0;
}

}

