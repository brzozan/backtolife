/*
 * cdc160.h	$Revision: 1.10 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __cdc160_h__
#define __cdc160_h__

#ifndef __arith_c1_h__
#include "arith_c1.h"
#endif

#ifndef __loader_h__
#include "loader.h"
#endif

#include <vector>

namespace cdc160
{
    struct Device;

    /**
     * Machine word - 12-bit value with arithmetic in one's component.
     */
    typedef arith_c1::value<unsigned int, 12> word;

    /**
     * Memory address - word with additional information about from which
     * memory block it comes.
     * Possible blocks:
     *  - none  - for "special" addressing - no block specified, block 0 used,
     *  - r     - relative addressing block,
     *  - d     - direct addressing block,
     *  - i     - indirect addressing block,
     *  - b     - buffer block.
     */
    class address : public word
    {
        public:
            enum block
            {
                block_none,
                block_r,
                block_d,
                block_i,
                block_b
            };

            address(block bl, const word &addr) : word(addr), m_block(bl) {}

            block   getBlock() const {  return m_block; }
            
        private:
            block   m_block;
    };

    /**
     * CDC-160A memory unit.
     */
    class memory
    {
        public:
            static const unsigned short BLOCK_SIZE  = 4096;

            memory(int blocks);
            ~memory();

            word    read(const address &adr) const;
            void    write(const address &adr, const word &value);

            int     getR() const    {   return m_regR;  }
            void    setR(int r)     {   m_regR  = r;    }
            int     getD() const    {   return m_regD;  }
            void    setD(int d)     {   m_regD  = d;    }
            int     getI() const    {   return m_regI;  }
            void    setI(int i)     {   m_regI  = i;    }
            int     getB() const    {   return m_regB;  }
            void    setB(int b)     {   m_regB  = b;    }
            
        private:
            int     getBlockIndex(address::block bl) const;

            int     m_regR;
            int     m_regD;
            int     m_regI;
            int     m_regB;
            
            int     m_blocks;
            word    **m_data;
    };
    
    /**
     * Control Data Corporation CDC-160A machine CPU emulator.
     */
    class CDC160 : public CodeMemoryInterface
    {
        public:
            enum addressing_mode
            {
                mode_direct     = 0,
                mode_indirect   = 1,
                mode_forward    = 2,
                mode_backward   = 3
            };
            
            /**
             * Create CDC-160A instance with given number of memory blocks.
             * Each block has 4096 words. Maximum of 8 blocks is supported.
             */
            CDC160(int memoryBlocks);
            
            virtual ~CDC160();

            /**
             * Load code from file.
             */
            void    loadCode(const char *fileName);
            
            /**
             * Perform single instruction step (note: not memory cycle step as
             * expected by step execution in original machine.
             */
            bool    step();
            
            /**
             * Set stop switches state. The 3 lowest bits are used. Bit should
             * be set to 1 if corresponding switch is enabled.
             */
            void    setStopSwitches(unsigned short state);
            
            /**
             * Set jump switches state. The 3 lowest bits are used. Bit should
             * be set to 1 if corresponding switch is enabled.
             */
            void    setJumpSwitches(unsigned short state);
            
            /**
             * Add external device to be used by CPU.
             */
            void    addDevice(Device *device);
            
            /**
             * Read program and/or data from tape starting from addr.
             * After that P will contain last address occupied by read data
             * and A will contain check sum of data modulus 2^12-1.
             * Paper tape reader must be connected and its input must be
             * connected to valid file!
             */
            void    readTape(unsigned short addr);
            
            // CodeMemoryInterface implementation
            
            virtual void    setCodeCell(unsigned int addr, unsigned int val);
            virtual void    setStartAddress(unsigned int addr);
            
            word    getA() const    {   return m_regA;      }
            word    getP() const    {   return m_regP;      }
            word    getBER() const  {   return m_regBER;    }
            word    getBXR() const  {   return m_regBXR;    }
            word    getZ() const    {   return m_regZ;      }
            word    getS() const    {   return m_regS;      }
            word    getF() const    {   return m_regF;      }
            word    getBFR() const  {   return m_regBFR;    }
            word    getAp() const   {   return m_regAp;     }
            word    getPSR() const  {   return m_regPSR;    }

        private:
            word    doInput();
            void    doOutput(const word &val);
            void    doExternal(const word &code);
            
            void    fetchInstruction();
            word    fetchG() const;
            bool    checkInterrupt();
            void    interrupt(unsigned short addr);

            word    doArithmetic(unsigned short opcode, const word &operand1, const word &operand2) const;
            
            address getAddress(addressing_mode mode, const word &extension, bool &advanceP) const;

            unsigned short  m_switchesStop;
            unsigned short  m_switchesJump;
            
            word    m_regA;
            word    m_regP;
            word    m_regBER;
            word    m_regBXR;
            word    m_regZ;
            word    m_regS;
            word    m_regF;
            word    m_regBFR;
            word    m_regAp;
            word    m_regPSR;
            bool    m_interruptLockout;
            bool    m_interrupts[4];

            memory  m_memory;

            std::vector<Device*>    m_devices;
    };
    
}

#endif /* __cdc160_h__ */

