/*
 * baby.cc	$Revision: 1.1 $	$Date: 2006/08/06 08:58:11 $	MB
 *
 * Copyright (c) 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <sstream>
#include <string>
#include <algorithm>
#include "baby.h"

baby::Baby::Baby()
{
}

baby::Baby::~Baby()
{
}

void baby::Baby::loadCode(const std::string &fileName)
{
    std::fstream    file(fileName.c_str(), std::ios::in);

    if (!file.fail()) {
        int         addr    = 0;
        std::string str;

        std::fill(m_memory, m_memory + 32, 0);

        m_a     = 0;
        m_ci    = -1;

        while (!std::getline(file, str).eof()) {
            std::stringstream   line(str, std::ios::in);
            std::string         op;
            int                 storeLine;
            unsigned int        opcode  = 0;
            bool                hasArg  = true;

            line >> op;

            if (op.length() == 0 || op[0] == '#') {
                continue;
            } else if (op == "JMP") {
                opcode  = 0x00;
            } else if (op == "JRP") {
                opcode  = 0x01;
            } else if (op == "LDN") {
                opcode  = 0x02;
            } else if (op == "STO") {
                opcode  = 0x03;
            } else if (op == "SUB") {
                opcode  = 0x04;
            } else if (op == "CMP") {
                opcode  = 0x06;
                hasArg  = false;
            } else if (op == "STOP") {
                opcode  = 0x07;
                hasArg  = false;
            } else if (op == ";") {
                opcode  = 0x00;
            }
            if (hasArg) {
                line >> storeLine;
            } else {
                storeLine   = 0;
            }
            m_memory[addr]  = (opcode << 13) | storeLine;
            ++addr;
        }
    }
}

bool baby::Baby::step()
{
    bool            result      = false;
    unsigned int    pi          = 0;
    unsigned int    opcode      = 0;
    unsigned int    storeLine   = 0;
    
    m_ci        = m_ci + 1;
    pi          = m_memory[m_ci & 0x1F];
    opcode      = (pi >> 13) & 0x07;
    storeLine   = pi & 0x1F;

    switch (opcode) {
    case 0x00:  // JMP
        m_ci    = m_memory[storeLine];
        break;
    case 0x01:  // JRP
        m_ci    += m_memory[storeLine];
        break;
    case 0x02:  // LDN
        m_a     = -m_memory[storeLine];
        break;
    case 0x03:  // STO
        m_memory[storeLine] = m_a;
        break;
    case 0x04:  // SUB
    case 0x05:
        m_a = m_a - m_memory[storeLine];
        break;
    case 0x06:  // CMP
        if (m_a & 0x80000000) {
            m_ci++;
        }
        break;
    case 0x07:  // STOP
        result  = true;
        break;
    }
    return result;
}

void baby::Baby::showState() const
{
    std::cout << "A: " << std::hex << m_a << " CI: " << std::hex << m_ci << " Memory:";
    for (int i = 0; i < 32; ++i) {
        std::cout << " " << std::hex << m_memory[i];
    }
    std::cout << std::endl;
}

