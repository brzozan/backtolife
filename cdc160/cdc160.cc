/*
 * cdc160.cc	$Revision: 1.18 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include "cdc160.h"
#include "device.h"

static bool TRACE   = false;

namespace cdc160
{

static inline word mul10(const word &x)
{
    word    mul2    = x + x;
    word    mul4    = mul2 + mul2;

    return mul4 + mul4 + mul2;
}
    
memory::memory(int blocks) : m_regR(0), m_regD(0), m_regI(0), m_regB(0),
                             m_blocks(blocks), m_data(NULL)
{
    m_data  = new word*[blocks];
    
    for (int i = 0; i < blocks; ++i) {
        m_data[i]   = new word[BLOCK_SIZE];
    }
}

memory::~memory()
{
    if (m_data != NULL) {
        for (int i = 0; i < m_blocks; ++i) {
            if (m_data[i] != NULL) {
                delete[] m_data[i];
            }
        }
        delete[] m_data;
    }
}

int memory::getBlockIndex(address::block bl) const
{
    int result  = 0;
    switch (bl) {
    case address::block_none:
        result  = 0;
        break;
    case address::block_r:
        result  = m_regR;
        break;
    case address::block_d:
        result  = m_regD;
        break;
    case address::block_i:
        result  = m_regI;
        break;
    case address::block_b:
        result  = m_regB;
        break;
    }
    return 0;
}

word memory::read(const address &adr) const
{
    word    result;
    int     block   = getBlockIndex(adr.getBlock());
    if (block >= 0 && block < m_blocks) {
        result  = m_data[block][(unsigned short)adr];
    }
    return result;
}

void memory::write(const address &adr, const word &value)
{
    int     block   = getBlockIndex(adr.getBlock());
    if (block >= 0 && block < m_blocks) {
        m_data[block][(unsigned short)(adr % BLOCK_SIZE)]  = value;
    }
}

CDC160::CDC160(int memoryBlocks) : m_switchesStop(0), m_switchesJump(0), m_interruptLockout(false),
                                   m_memory(memoryBlocks)
{
    for (int i = 0; i < 4; ++i) {
        m_interrupts[i] = false;
    }
}

CDC160::~CDC160()
{
}

void CDC160::fetchInstruction()
{
    m_regF  = m_memory.read(address(address::block_r, m_regP));
}

word CDC160::fetchG() const
{
    return m_memory.read(address(address::block_r, m_regP + word(1)));
}

void CDC160::interrupt(unsigned short addr)
{
    if (addr == 00010 || addr == 00020 || addr == 00030 || addr == 00040) {
        m_interrupts[(addr - 00010) >> 3] = true;
    }
}

bool CDC160::checkInterrupt()
{
    bool    result  = false;
    
    if (!m_interruptLockout) {
        for (int i = 0; i < 4; ++i) {
            if (m_interrupts[i]) {
                unsigned short  addr    = (i << 3) + 00010;
                
                // store P in (d)addr
                m_memory.write(address(address::block_d, addr), m_regP);
                // jump to (r)addr + 1
                m_regP  = word(addr) + word(1);

                m_interruptLockout  = true;
                m_interrupts[i]     = false;
                result              = true;
                break;
            }
        }
    }
    return result;
}

address CDC160::getAddress(CDC160::addressing_mode mode, const word &extension, bool &advanceP) const
{
    advanceP    = false;

    switch (mode) {
    case mode_direct:
        // direct - extension is address in direct block
        return address(address::block_d, extension);
    case mode_indirect:
        if ((unsigned short)extension == 0) {
            // memory - next word is address in indirect block
            // P advances by 2
            advanceP    = true;
            return address(address::block_i, fetchG());
        } else {
            // indirect - extension is address in direct block, whose contents
            // are to be used as address in indirect block
            return address(address::block_i, m_memory.read(address(address::block_d, extension)));
        }
    case mode_forward:
        if ((unsigned short)extension == 0) {
            // constant - address follows immediately P
            // P advances by 2
            advanceP    = true;
            return address(address::block_r, m_regP + word(1));
        } else {
            // forward relative - address is P + extension
            return address(address::block_r, m_regP + extension);
        }
    case mode_backward:
        if ((unsigned short)extension == 0) {
            // specific - word 07777 in block 0
            return address(address::block_none, word(07777));
        } else {
            // backward relative - address is P - extension
            return address(address::block_r, m_regP - extension);
        }
    }
    // FIXME do some error handling here...
    return address(address::block_none, word(00000));
}

word CDC160::doArithmetic(unsigned short opcode, const word &operand1, const word &operand2) const
{
    word    result  = operand1;

    switch (opcode) {
    case 002:
        // LPx
        result  = operand1 & operand2;
        break;
    case 003:
        // SCx
        result  = operand1 ^ operand2;
        break;
    case 004:
        // LDx
        result  = operand2;
        break;
    case 005:
        // LCx
        result  = -operand2;
        break;
    case 006:
        // ADx
        result  = operand1 + operand2;
        break;
    case 007:
        // SBx
        result  = operand1 - operand2;
        break;
    }
    return result;
}

bool CDC160::step()
{
    bool            advanceP    = false;
    bool            stop        = false;
    word            nextP       = m_regP + word(1);
    word            extension;
    unsigned short  opcode;
    unsigned short  instruction;
    unsigned short  ext;

    // this will jump somewhere if interrupt occurred.
    // is it really OK?
    checkInterrupt();
    
    if (TRACE) {
        printf("%05o: %05o (%05o) :: ", (unsigned short)m_regP, (unsigned short)m_regF, (unsigned short)fetchG());
    }
    
    instruction = (unsigned short)m_regF;
    opcode      = (unsigned short)m_regF >> 6;
    extension   = m_regF & word(00077);
    ext         = (unsigned short)extension;

    switch (opcode) {
    case 000: {
        if (ext == 000) {
            // ERR
            stop    = true;
        } else if (ext < 010) {
            // NOP
        } else {
            // bank set
            unsigned short  value   = ext & 007;
            unsigned short  banks   = ext & 070;

            if ((banks & 010) != 0) {
                m_memory.setR(value);

                // changing R bank causes jump to address from A
                nextP       = m_regA;
                advanceP    = false;
            }
            if ((banks & 020) != 0) {
                m_memory.setI(value);
            }
            if ((banks & 040) != 0) {
                m_memory.setD(value);
            }
        }
        break;
    }
    case 001: {
        if (ext == 000) {
            // BLS
            // FIXME do some "buffer in operation" checking
            // FIXME P should advance right after BFR = A assignment
            m_regBFR    = m_regA;
            do {
                m_memory.write(address(address::block_b, m_regBER), m_regBFR);
                m_regBER    = m_regBER + word(1);
            } while (m_regBER != m_regBXR);
            advanceP    = true; // to skip G
        } else if (ext == 001) {
            // PTA
            m_regA      = m_regP;
        } else if (ext == 002) {
            // LS1
            m_regA      = m_regA.rotate_left(1);
        } else if (ext == 003) {
            // LS2
            m_regA      = m_regA.rotate_left(2);
        } else if (ext == 004) {
            // CBC
            // FIXME currently do nothing
        } else if (ext == 005) {
            // ATE
            // FIXME add some "device busy" checking
            m_regBER    = m_regA;
            advanceP    = true; // to skip G
        } else if (ext == 006) {
            // ATX
            // FIXME add some "device busy" checking
            m_regBXR    = m_regA;
            advanceP    = true; // to skip G
        } else if (ext == 007) {
            // ETA
            m_regA      = m_regBER;
        } else if (ext == 010) {
            // LS3
            m_regA      = m_regA.rotate_left(3);
        } else if (ext == 011) {
            // LS6
            m_regA      = m_regA.rotate_left(6);
        } else if (ext == 012) {
            // MUT
            m_regA  = mul10(m_regA);
        } else if (ext == 013) {
            // MUH
            m_regA  = mul10(mul10(m_regA));
        } else if (ext == 014) {
            // RS1
            m_regA      = m_regA.rotate_right(1);
        } else if (ext == 015) {
            // RS2
            m_regA      = m_regA.rotate_right(2);
        } else if (ext == 020) {
            // CIL
            m_interruptLockout  = false;
        } else if (ext == 030) {
            // CTA
            unsigned short  r   = ((unsigned short)m_memory.getR() & 007) << 0;
            unsigned short  i   = ((unsigned short)m_memory.getI() & 007) << 3;
            unsigned short  d   = ((unsigned short)m_memory.getD() & 007) << 6;
            unsigned short  b   = ((unsigned short)m_memory.getB() & 007) << 9;

            m_regA  = word(r | i | d | b);
        } else if ((ext & 070) == 040) {
            // SBU
            m_memory.setB(ext & 007);
        } else if ((ext & 070) == 050) {
            // STP
            m_memory.write(address(address::block_d, extension), m_regP);
        } else if ((ext & 070) == 060) {
            // STE
            m_memory.write(address(address::block_d, extension), m_regBER);
            m_regBER    = m_regA;
        }
        break;
    }
    case 002:
    case 003:
    case 004:
    case 005:
    case 006:
    case 007: {
        // arithmetical and logical operations without address
        m_regA  = doArithmetic(opcode, m_regA, extension);
        break;
    }
    case 010:
    case 011:
    case 012:
    case 013:
    case 014:
    case 015:
    case 016:
    case 017:
    case 020:
    case 021:
    case 022:
    case 023:
    case 024:
    case 025:
    case 026:
    case 027:
    case 030:
    case 031:
    case 032:
    case 033:
    case 034:
    case 035:
    case 036:
    case 037: {
        // arithmetical and logical functions involving memory
        // lowest 2 bits of opcode indicate addressing mode, higher 4 indicate
        // operation and are the same as for immediate operations
        addressing_mode mode    = (addressing_mode)(opcode & 003);
        unsigned short  op      = opcode >> 2;
        address         addr    = getAddress(mode, extension, advanceP);

        m_regS  = addr;
        m_regA  = doArithmetic(op, m_regA, m_memory.read(addr));
        break;
    }
    case 040:
    case 041:
    case 042:
    case 043: {
        // STx
        // lowest 2 bits indicate addressing mode
        addressing_mode mode    = (addressing_mode)(opcode & 003);
        address         addr    = getAddress(mode, extension, advanceP);

        m_regS  = addr;
        m_memory.write(addr, m_regA);
        break;
    }
    case 044:
    case 045:
    case 046:
    case 047: {
        // SRx
        // lowest 2 bits indicate addressing mode
        addressing_mode mode    = (addressing_mode)(opcode & 003);
        address         addr    = getAddress(mode, extension, advanceP);

        m_regS  = addr;
        m_regA  = m_memory.read(addr);
        m_regA  = m_regA.rotate_left(1);
        m_memory.write(addr, m_regA);
        break;
    }
    case 050:
    case 051:
    case 052:
    case 053: {
        // RAx
        // lowest 2 bits indicate addressing mode
        addressing_mode mode    = (addressing_mode)(opcode & 003);
        address         addr    = getAddress(mode, extension, advanceP);

        m_regS  = addr;
        m_regA  = m_regA + m_memory.read(addr);
        m_memory.write(addr, m_regA);
        break;
    }
    case 054:
    case 055:
    case 056:
    case 057: {
        // AOx
        // lowest 2 bits indicate addressing mode
        addressing_mode mode    = (addressing_mode)(opcode & 003);
        address         addr    = getAddress(mode, extension, advanceP);

        m_regS  = addr;
        m_regA  = m_memory.read(addr) + word(1);
        m_memory.write(addr, m_regA);
        break;
    }
    case 060: {
        // ZJF
        address addr    = getAddress(mode_forward, extension, advanceP);
        if ((unsigned short)m_regA == 00000) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 061: {
        // NZF
        address addr    = getAddress(mode_forward, extension, advanceP);
        if ((unsigned short)m_regA != 00000) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 062: {
        // PJF
        address addr    = getAddress(mode_forward, extension, advanceP);
        if (m_regA.is_positive()) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 063: {
        // NJF
        address addr    = getAddress(mode_forward, extension, advanceP);
        if (m_regA.is_negative()) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 064: {
        // ZJB
        address addr    = getAddress(mode_backward, extension, advanceP);
        if ((unsigned short)m_regA == 00000) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 065: {
        // NZB
        address addr    = getAddress(mode_backward, extension, advanceP);
        if ((unsigned short)m_regA != 00000) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 066: {
        // PJB
        address addr    = getAddress(mode_backward, extension, advanceP);
        if (m_regA.is_positive()) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 067: {
        // NJB
        address addr    = getAddress(mode_backward, extension, advanceP);
        if (m_regA.is_negative()) {
            nextP       = addr;
            advanceP    = false;
        }
        break;
    }
    case 070: {
        // JPI
        nextP       = getAddress(mode_indirect, extension, advanceP);
        advanceP    = false;
        break;
    }
    case 071: {
        if (ext == 000) {
            // JPR
            address addr    = address(address::block_r, fetchG());

            m_memory.write(addr, m_regP + word(2));
            m_regS      = addr;
            nextP       = fetchG() + word(1);
            advanceP    = false;
        } else {
            // JFI
            nextP       = m_memory.read(getAddress(mode_forward, extension, advanceP));
            advanceP    = false;
        }
        break;
    }
    case 072: {
        if (ext == 000) {
            // IBI
            advanceP    = true; // to skip G
        } else {
            // INP
            word    fwa = m_memory.read(getAddress(mode_forward, extension, advanceP));
            word    lwa = fetchG();

            do {
                word    data    = doInput();
                
                m_memory.write(address(address::block_i, fwa), data);
                m_regS  = fwa;
                fwa     = fwa + word(1);
            } while (fwa != lwa);
            
            advanceP    = true; // to skip G
        }
        break;
    }
    case 073: {
        if (ext == 000) {
            // IBO
            advanceP    = true; // to skip G
        } else {
            // OUT
            word    fwa = m_memory.read(getAddress(mode_forward, extension, advanceP));
            word    lwa = fetchG();

            do {
                word    data    = m_memory.read(address(address::block_i, fwa));
                
                m_regS  = fwa;
                fwa     = fwa + word(1);
                doOutput(data);
            } while (fwa != lwa);
            
            advanceP    = true; // to skip G
        }
        break;
    }
    case 074: {
        // OTN
        doOutput(extension);
        break;
    }
    case 075: {
        // EXC/EXF
        address addr    = getAddress(mode_forward, extension, advanceP);
        word    code    = m_memory.read(addr);
        
        m_regS  = addr;
        doExternal(code);
        break;
    }
    case 076: {
        if (ext == 000) {
            // INA
            m_regA  = doInput();
        } else if (ext == 077) {
            // OTA
            doOutput(m_regA);
        } else {
            // HWI
            address addr    = getAddress(mode_indirect, extension, advanceP);
            word    tmp     = (m_memory.read(addr) & word(07700)) | (m_regA & word(00077));

            m_regS  = addr;
            m_memory.write(addr, tmp);
        }
        break;
    }
    case 077: {
        if (ext == 000 || ext == 077) {
            // HLT
            stop    = true;
        } else if (ext < 007) {
            // SLS
            if ((m_switchesStop & ext) != 0) {
                stop    = true;
            }
        } else if ((ext & 007) == 0) {
            // SLJ
            if ((m_switchesJump & (ext >> 3)) != 0) {
                nextP       = fetchG();
                advanceP    = false;
            } else {
                advanceP    = true; // to skip G
            }
        } else {
            // SJS
            // do the "jump" part
            if ((m_switchesJump & (ext >> 3)) != 0) {
                nextP       = fetchG();
                advanceP    = false;
            } else {
                advanceP    = true; // to skip G
            }
            // dirty trick to update P before instruction
            // is finished
            if (advanceP) {
                nextP   = nextP + word(1);
            }
            m_regP      = nextP;
            advanceP    = false;
            // after that P == nextP and advanceP == false,
            // so P update at the end of function will do nothing
            
            // do the "stop" part
            if ((m_switchesStop & (ext & 007)) != 0) {
                stop    = true;
            }
        }
        break;
    }
    }

    if (advanceP) {
        nextP   = nextP + word(1);
    }
    m_regP  = nextP;

    fetchInstruction();

    if (TRACE) {
        printf("A=%05o S=%05o P=%05o\n", (unsigned short)m_regA, (unsigned short)m_regS, (unsigned short)m_regP);
    }
    
    return stop;
}

void CDC160::setCodeCell(unsigned int addr, unsigned int val)
{
    m_memory.write(address(address::block_r, addr), val);
}

void CDC160::setStartAddress(unsigned int addr)
{
    m_regP  = addr;
    // fetch instruction to be executed in next step
    fetchInstruction();
}

void CDC160::addDevice(Device *device)
{
    m_devices.push_back(device);
}

word CDC160::doInput()
{
    word    result(00000);

    for (std::vector<Device*>::const_iterator it = m_devices.begin(); it != m_devices.end(); ++it) {
        if ((*it)->active()) {
            result  = (*it)->input();
            break;
        }
    }
    //printf("< %05o\n", (unsigned short)result);
    return result;
}

void CDC160::doOutput(const word &val)
{
    if (TRACE) {
        printf("      %05o (%c)\n", (unsigned short)val, (char)(unsigned short)val);
    }
    
    for (std::vector<Device*>::const_iterator it = m_devices.begin(); it != m_devices.end(); ++it) {
        if ((*it)->active()) {
            (*it)->output((unsigned short)val);
            break;
        }
    }
}

void CDC160::doExternal(const word &code)
{
    if (TRACE) {
        printf("(ext) %05o\n", (unsigned short)code);
    }
    
    for (std::vector<Device*>::const_iterator it = m_devices.begin(); it != m_devices.end(); ++it) {
        (*it)->command((unsigned short)code);
    }
}

void CDC160::setStopSwitches(unsigned short state)
{
    m_switchesStop  = (state & 00007);
}

void CDC160::setJumpSwitches(unsigned short state)
{
    m_switchesJump  = (state & 00007);
}

void CDC160::loadCode(const char *fileName)
{
    Loader::loadCode(fileName, this);
}

void CDC160::readTape(unsigned short addr)
{
    unsigned short  hi      = 0;
    unsigned short  lo      = 0;
    unsigned short  data    = 0;
    
    m_regA  = 0;
    m_regP  = addr;

    // select tape reader
    doExternal(04102);
    // read until first word with 7-th bit set is encountered
    // NOTE: this will hang if error occurrs...
    while ((hi & 0100) == 0) {
        hi  = doInput();
    }
    lo  = doInput();
    while (true) {
        if ((hi & 0100) == 0 || (lo & 0100) != 0) {
            break;
        }
        data    = ((hi & 0077) << 6) | lo;
        m_memory.write(address(address::block_r, m_regP), data);
        m_regA  = m_regA + word(data);
        m_regP  = m_regP + word(1);
        
        hi      = doInput();
        if ((hi & 0100) != 0) {
            lo  = doInput();
        }
    }
}

}

