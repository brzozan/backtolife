#! /bin/env python

#
# tapify.py	$Revision: 1.2 $	$Date: 2006/08/06 07:13:00 $	MB
#
# Copyright (c) 2004, 2005, 2006 Michal Brzozowski
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys

if len(sys.argv) != 2:
    print 'usage: %s <filename>' % sys.argv[0]
    sys.exit(1)

memory  = [0] * 4096
minaddr = 00000
maxaddr = 00000
addr    = 00000
f       = open(sys.argv[1], 'rU')
for line in f:
    line    = line.strip()
    if line.startswith('*') or line == '':
        pass
    else:
        words   = line.split()

        for w in words:
            if w[0] == ';':
                addr    = int(w[1:], 8)
            else:
                memory[addr]    = int(w, 8)
                addr            = addr + 1
                
                if addr < minaddr:
                    minaddr = addr
                if addr > maxaddr:
                    maxaddr = addr

def encodeRow(data, levels):
    s   = '|'
    for i in xrange(levels - 1, -1, -1):
        if i == 2:
            s   = s + '.'
        if (data & (1 << i)) != 0:
            s   = s + 'o'
        else:
            s   = s + ' '
    s   = s + '|'
    return s

for i in xrange(minaddr, maxaddr):
    hi  = (memory[i] >> 6) + 0100
    lo  = memory[i] & 00077

    print encodeRow(hi, 7)
    print encodeRow(lo, 7)
# additional 0 to mark end of data
print encodeRow(0, 7)
