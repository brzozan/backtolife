/*
 * device.h	$Revision: 1.2 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __device_h__
#define __device_h__

namespace cdc160
{

    /**
     * CDC-160 external device interface.
     */
    struct Device
    {
        public:
            /**
             * EXC/EXF has been executed. If device recognizes this command it
             * should execute it. If not, it should disable itself.
             * All devices on start-up should be in inactive state.
             */
            virtual void            command(unsigned short cmd) = 0;
            /**
             * Check if device is active.
             */
            virtual bool            active()                    = 0;
            /**
             * Output has been executed. This method will be called only if device
             * reports to be active.
             */
            virtual void            output(unsigned short data) = 0;
            /**
             * Input has been requested. Device should return data that have been
             * read. This method will be called only if device reports to be active.
             * TODO what about some kind of error handling..?
             */
            virtual unsigned short  input()                     = 0;
            /**
             * Device status is needed. This method will be called only if device
             * reports to be active.
             */
            virtual unsigned short  status()                    = 0;

        protected:
            virtual ~Device()   {};
    };

}

#endif /* __device_h__ */

