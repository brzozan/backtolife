/*
 * loader.cc	$Revision: 1.4 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include "loader.h"

/*
 * File format:
 * 
 * *=startaddr
 * ;addr val1 val2 val3 val4 ...
 *
 * All values octal
 */

static const char *loader_get_value(const char *p, unsigned int *result_ptr)
{
    unsigned int    result  = 0;

    while (*p && std::isspace(*p)) {
        p++;
    }
    while (*p && !std::isspace(*p)) {
        if (*p >= '0' && *p <= '7') {
            result  = (result << 3) + (*p - '0');
        }
        p++;
    }
    *result_ptr = result;
    return *p != 0 ? p : NULL;
}

bool Loader::loadCode(const char *fileName, CodeMemoryInterface *target)
{
    bool            result          = true;
    unsigned int    addr            = 0;
    unsigned int    start_address   = 0;
    std::fstream    file(fileName, std::ios::in);
    std::string     str;


    if (!file.fail()) {
        while (!std::getline(file, str).eof()) {
            const char  *p  = str.c_str();

            while (*p && std::isspace(*p)) {
                p++;
            }
            if (*p == '*') {
                loader_get_value(p + 1, &start_address);
            } else if (*p == ';') {
                unsigned int    tmp;
                
                p   = loader_get_value(p + 1, &addr);

                while (p != NULL) {
                    p   = loader_get_value(p, &tmp);

                    target->setCodeCell(addr, tmp);
                    addr++;
                }
            }
        }
        target->setStartAddress(start_address);
    } else {
        result  = false;
    }
    return result;
}

