/*
 * main.cc	$Revision: 1.8 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <iostream>
#include <fstream>
#include "cdc160.h"
#include "plotter.h"
#include "tape.h"
#include "typewriter.h"

int main(int argc, char **argv)
{
    if (argc > 1) {
        cdc160::CDC160      cdc(8);             // CDC-160 with 8 memory banks (32768 words)
        cdc160::TapeReader  reader;             // 350 paper tape reader
        cdc160::TapePuncher puncher;            // BRPE-11 paper tape puncher with 8-level tape
        cdc160::Typewriter  typewriter1(true);  // Primary typewriter
        cdc160::Typewriter  typewriter2(false); // Secondary typewriter
        cdc160::Plotter     plotter;            // 165 Plotter

        puncher.setChannels(7);
        
        cdc.addDevice(&reader);
        cdc.addDevice(&puncher);
        cdc.addDevice(&typewriter1);
        cdc.addDevice(&typewriter2);
        cdc.addDevice(&plotter);
        
        // check for -t switch, which implies reading from tape dump
        // FIXME data is read always from address 00000
        if (argc == 3 && argv[1][0] == '-' && argv[1][1] == 't') {
            std::fstream    file(argv[2], std::ios::in);
            
            if (!file.fail()) {
                reader.setStream(&file);
                cdc.readTape(00000);
                cdc.setStartAddress(00000);
                reader.setStream(NULL);
            } else {
                std::cout << "error: failed to open file " << argv[2] << std::endl;
            }
        } else {
            cdc.loadCode(argv[1]);
        }
        
        while (!cdc.step());

        plotter.flush();
    } else {
        std::cout << "usage: " << argv[0] << " <filename.cdc> | -t <tapefile>" << std::endl;
    }
    return 0;
}


