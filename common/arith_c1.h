/*
 * arith_c1.h	$Revision: 1.2 $	$Date: 2004/09/11 16:44:36 $	MB
 *
 * Copyright (c) 2004 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __arith_c1_h__
#define __arith_c1_h__

/*
 * n-bit one's complement value storage and arithmetic.
 */
namespace arith_c1
{

    /**
     * Class storing values for n-bit one's complement computations.
     *
     * _storage - storage on which data will be held. This should be
     *          unsigned type of bit size at least the same as number
     *          of bits on which computations should be done.
     * _bits    - number of bits on which computations will be performed.
     */
    template<typename _storage, int _bits> class value
    {
        _storage    v;
        
        public:
            value() : v(0) {}

            value(_storage x) : v(x & bit_mask()) {}

            value(const value &rhs) : v(rhs.v) {}

            value &operator=(const value &rhs) {
                v   = rhs.v;
                return *this;
            }
       
            /**
             * Returns _bits-bits long mask of 1's.
             */     
            static _storage bit_mask() {
                return ((_storage)-1 >> (sizeof(_storage) * 8 - _bits));
            }

            value negate() const {
                return value(v ^ bit_mask());
            }
            
            value operator+(const value &rhs) const {
                return value((v + rhs.v) % bit_mask());
            }
            
            value operator-() const {
                return negate();
            }
            
            value operator-(const value &rhs) const {
                return *this + (-rhs);
            }
            
            bool operator==(const value &rhs) const {
                return v == rhs.v;
            }
            
            value operator&(const value &rhs) const {
                return value(v & rhs.v);
            }
            
            value operator|(const value &rhs) const {
                return value(v | rhs.v);
            }
            
            value operator^(const value &rhs) const {
                return value(v ^ rhs.v);
            }

            value operator>>(int n) const {
                return value(v >> n);
            }

            value operator<<(int n) const {
                // value constructor should crop unnecessary bits
                return value(v << n);
            }
            
            value rotate_left(int n) const {
                            n       = n % _bits;
                _storage    moved   = v << n;
                _storage    carry   = v >> (_bits - n);

                return value(moved | carry);
            }
            
            value rotate_right(int n) const {
                            n       = n % _bits;
                _storage    moved   = v >> n;
                _storage    carry   = v << (_bits - n);

                return value(moved | carry);
            }
            
            bool is_zero() const {
                return (v == 0 || v == bit_mask());
            }

            bool is_negative() const {
                return ((v & ((_storage)1 << (_bits - 1))) != 0);
            }

            bool is_positive() const {
                return !is_negative();
            }
            
            operator _storage() const {
                return v;
            }
    };

}

#endif /* __arith_c1_h__ */

