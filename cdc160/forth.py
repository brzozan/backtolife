#! /bin/env python

#
# forth.py	$Revision: 1.6 $	$Date: 2006/08/06 07:13:00 $	MB
#
# Copyright (c) 2004, 2005, 2006 Michal Brzozowski
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import sys

push    = [ "           STI spdata",
            "           AOD spdata"
          ]
pop     = [ "           LCN 1",
            "           RAD spdata",
            "           LDI spdata"
          ]

def isnumber(s):
    for c in s:
        if c not in '0123456789':
            return 0
    return 1

def printlist(l):
    for ll in l:
        print ll

prolog  = [ "           .ORIGIN 00000",
            "           .WORD   00000",
            "tmp:       .WORD   00000",
            "spdata:    .WORD   07000",
            "spreturn:  .WORD   07400"
          ]
code    = [ "           .START",
            "           EXC     04104"
          ]

def wordSource(fileName):
    f   = open(fileName, 'rU')
    for line in f:
        words   = line.strip().split()
        for word in words:
            yield word

if len(sys.argv) != 2:
    print 'usage: %s <filename>' % sys.argv[0]
    sys.exit(1)

source      = wordSource(sys.argv[1])
variables   = []

try:
    while 1:
        w   = source.next()
        if isnumber(w):
            code    = code + ["           LDN %03o" % (int(w) & 077)]
            code    = code + push
        elif w == '+':
            code    = code + pop
            code    = code + ["           STD tmp"]
            code    = code + pop
            code    = code + ["           ADD tmp"]
            code    = code + push
        elif w == '-':
            code    = code + pop
            code    = code + ["           STD tmp"]
            code    = code + pop
            code    = code + ["           SBD tmp"]
            code    = code + push
        elif w == '.':
            code    = code + pop
            code    = code + ["           OTA"]
        elif w == '@':
            code    = code + pop
            code    = code + ["           STD tmp",
                              "           LDI tmp"]
            code    = code + push
        elif w == '!':
            code    = code + pop
            code    = code + ["           STD tmp"]
            code    = code + pop
            code    = code + ["           STI tmp"]
        elif w == 'VARIABLE':
            name    = source.next()
            variables.append(name)
            prolog  = prolog + ["%s:%s.WORD   00040" % (name, " " * (10 - len(name)))]
        elif w in variables:
            code    = code + ["           LDC %s" % w]
            code    = code + push
except StopIteration:
    pass

code    = code + ["           HLT"]

printlist(prolog)
printlist(code)

