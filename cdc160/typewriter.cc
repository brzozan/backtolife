/*
 * typewriter.cc	$Revision: 1.4 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2004, 2005, 2006 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include "typewriter.h"

namespace cdc160
{

Typewriter::Typewriter(bool primaryDevice) : m_active(false), m_inputMode(false),
    m_primaryDevice(primaryDevice), m_streamIn(&std::cin), m_streamOut(&std::cout)
{
}

void Typewriter::setStreamIn(std::istream *strIn)
{
    if (strIn != NULL) {
        m_streamIn = strIn;
    } else {
        m_streamIn = &std::cin;
    }
}

void Typewriter::setStreamOut(std::ostream *strOut)
{
    if (strOut != NULL) {
        m_streamOut = strOut;
    } else {
        m_streamOut = &std::cout;
    }
}

void Typewriter::command(unsigned short cmd)
{
    if (m_primaryDevice) {
        if (cmd == 04210) {
            m_active    = true;
            m_inputMode = false;
        } else if (cmd == 04220) {
            m_active    = true;
            m_inputMode = true;
        } else if (cmd == 04240) {
            // request for status - currently ignored
        } else {
            m_active    = false;
        }
    } else {
        if (cmd == 04310) {
            m_active    = true;
            m_inputMode = false;
        } else if (cmd == 04320) {
            m_active    = true;
            m_inputMode = true;
        } else if (cmd == 04340) {
            // request for status - currently ignored
        } else {
            m_active    = false;
        }
    }
}

bool Typewriter::active()
{
    return m_active;
}

void Typewriter::output(unsigned short data)
{
    if (m_active && !m_inputMode) {
        char    chr = data & 00077;

        if (chr == 057) {
            m_upperCase = false;
        } else if (chr == 047) {
            m_upperCase = true;
        } else {
            *m_streamOut << translate(chr);
        }
    }
}

unsigned short Typewriter::input()
{
    unsigned short  result  = 00000;
    
    if (m_active && m_inputMode) {
        while (result == 00000 && !m_streamIn->eof()) {
            int chr = m_streamIn->get();

            result  = untranslate((char)chr);
        }
    }
    return result;
}

unsigned short Typewriter::status()
{
    // report ready
    return 00000;
}

static char toascii[][2]    = {
    // 000
    {   '\0',   '\0'    },
    {    't',    'T'    },
    {    '=',   0xf7    },  // divide sign
    {    'o',    'O'    },
    {    ' ',    ' '    },
    {    'h',    'H'    },
    {    'n',    'N'    },
    {    'm',    'M'    },
    // 010
    {   '\0',   '\0'    },
    {    'l',    'L'    },
    {    'r',    'R'    },
    {    'g',    'G'    },
    {    'i',    'I'    },
    {    'p',    'P'    },
    {    'c',    'C'    },
    {    'v',    'V'    },
    // 020
    {    'e',    'E'    },
    {    'z',    'Z'    },
    {    'd',    'D'    },
    {    'b',    'B'    },
    {    's',    'S'    },
    {    'y',    'Y'    },
    {    'f',    'F'    },
    {    'x',    'X'    },
    // 030
    {    'a',    'A'    },
    {    'w',    'W'    },
    {    'j',    'J'    },
    {    '8',   '\0'    },  // 1/2
    {    'u',    'U'    },
    {    'q',    'Q'    },
    {    'k',    'K'    },
    {    '9',    '('    },
    // 040
    {    ',',    ','    },
    {   '\0',   '\0'    },
    {    '.',    '.'    },
    {   '\0',   '\0'    },
    {    '/',    '?'    },
    {   '\n',   '\n'    },
    {    '+',   '\0'    },  // degree sign
    {   '\0',   '\0'    },
    // 050
    {    ';',    ':'    },
    {   '\t',   '\t'    },
    {    '-',    '_'    },
    {   '\0',   '\0'    },
    {   '\'',    '"'    },
    {   '\0',   '\0'    },
    {    '0',    ')'    },
    {   '\0',   '\0'    },
    // 060
    {    '7',    '&'    },
    {   '\0',   '\0'    },
    {    '4',    '$'    },
    {   '\0',   '\0'    },
    {    '3',    '#'    },
    {   '\0',   '\0'    },
    {    '5',    '%'    },
    {   '\0',   '\0'    },
    // 070
    {    '2',    '@'    },
    {   '\0',   '\0'    },
    {    '6',   '\0'    },  // cent sign
    {   '\0',   '\0'    },
    {    '1',    '*'    },
    {   '\0',   '\0'    },
    {   '\0',   '\0'    },
    {   '\0',   '\0'    },
};

char Typewriter::translate(char chr) const
{
    if (m_upperCase) {
        return toascii[(int)chr][1];
    } else {
        return toascii[(int)chr][0];
    }
}

// FIXME only lowercase characters are recognized
char Typewriter::untranslate(char chr) const
{
    char    result  = 0;

    for (unsigned int i = 0; i < sizeof(toascii) / sizeof(toascii[0]); ++i) {
        if (toascii[i][0] == chr) {
            result  = (char)i;
            break;
        }
    }
    return result;
}

}

