/*
 * mainwindow.cc	$Revision: 1.1 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2005 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#include <wx/wx.h>
#include <wx/sizer.h>
#include <wx/timer.h>
#include "mainwindow.h"
#include "cdc160.h"

#define CMD_STEP    0x1000
#define CMD_RUN     0x1001
#define CMD_STOP    0x1002

#define CMD_TIMER   0x1003

class MainWindow::Impl
{
    public:
        Impl();
        ~Impl();

        void    createCDC();
        void    destroyCDC();
        void    updateControls();
        
        cdc160::CDC160  *cdc;

        int             banks;
        
        wxStaticText    *textA;
        wxStaticText    *textP;
        wxStaticText    *textS;
        wxStaticText    *textF;
        wxStaticText    *textBER;
        wxStaticText    *textBXR;
        wxStaticText    *textBFR;
        wxStaticText    *textPSR;

        wxTimer         *timerRun;
};

MainWindow::Impl::Impl() : cdc(NULL), banks(8)
{
}

MainWindow::Impl::~Impl()
{
    if (cdc != NULL) {
        delete cdc;
    }
}

void MainWindow::Impl::createCDC()
{
    destroyCDC();

    cdc = new cdc160::CDC160(banks);
}

void MainWindow::Impl::destroyCDC()
{
    if (cdc != NULL) {
        delete cdc;
        cdc = NULL;
    }
}

void MainWindow::Impl::updateControls()
{
    textA->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getA()));
    textP->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getP()));
    textS->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getS()));
    textF->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getF()));
    
    textBER->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getBER()));
    textBXR->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getBXR()));
    textBFR->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getBFR()));
    textPSR->SetLabel(wxString::Format(_("%04o"), (unsigned short)cdc->getPSR()));
}

BEGIN_EVENT_TABLE(MainWindow, wxFrame)
    EVT_BUTTON(CMD_STEP,    MainWindow::onStep)
    EVT_BUTTON(CMD_RUN,     MainWindow::onRun)
    EVT_BUTTON(CMD_STOP,    MainWindow::onStop)
    EVT_TIMER(CMD_TIMER,    MainWindow::onTimer)
END_EVENT_TABLE()

MainWindow::MainWindow() : wxFrame(NULL, -1, _("CDC-160A"))
{
    impl    = new Impl;

    wxFlexGridSizer *sizer  = new wxFlexGridSizer(8, 8, 8);

    impl->textA     = new wxStaticText(this, -1, _("0000"));
    impl->textP     = new wxStaticText(this, -1, _("0000"));
    impl->textS     = new wxStaticText(this, -1, _("0000"));
    impl->textF     = new wxStaticText(this, -1, _("0000"));
    impl->textBER   = new wxStaticText(this, -1, _("0000"));
    impl->textBXR   = new wxStaticText(this, -1, _("0000"));
    impl->textBFR   = new wxStaticText(this, -1, _("0000"));
    impl->textPSR   = new wxStaticText(this, -1, _("0000"));
    
    sizer->Add(new wxStaticText(this, -1, _("A")));
    sizer->Add(impl->textA);
    sizer->Add(new wxStaticText(this, -1, _("P")));
    sizer->Add(impl->textP);
    sizer->Add(new wxStaticText(this, -1, _("S")));
    sizer->Add(impl->textS);
    sizer->Add(new wxStaticText(this, -1, _("F")));
    sizer->Add(impl->textF);
    
    sizer->Add(new wxStaticText(this, -1, _("BER")));
    sizer->Add(impl->textBER);
    sizer->Add(new wxStaticText(this, -1, _("BXR")));
    sizer->Add(impl->textBXR);
    sizer->Add(new wxStaticText(this, -1, _("BFR")));
    sizer->Add(impl->textBFR);
    sizer->Add(new wxStaticText(this, -1, _("PSR")));
    sizer->Add(impl->textPSR);
    
    sizer->Add(new wxButton(this, CMD_STEP, _("&Step")));
    sizer->Add(new wxButton(this, CMD_RUN, _("&Run")));
    sizer->Add(new wxButton(this, CMD_STOP, _("S&top")));
    
    SetSizer(sizer);
    sizer->SetSizeHints(this);

    impl->timerRun  = new wxTimer(this, CMD_TIMER);
    
    impl->createCDC();
    impl->cdc->loadCode("bench.cdc");
    impl->updateControls();
}

MainWindow::~MainWindow()
{
    delete impl;
}

void MainWindow::onStep(wxCommandEvent &event)
{
    impl->cdc->step();
    impl->updateControls();
}

void MainWindow::onRun(wxCommandEvent &event)
{
    impl->timerRun->Start(200);
}

void MainWindow::onStop(wxCommandEvent &event)
{
    impl->timerRun->Stop();
}

void MainWindow::onTimer(wxTimerEvent &event)
{
    impl->cdc->step();
    impl->updateControls();
}

