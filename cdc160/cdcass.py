#!/usr/bin/python

#
# cdcass.py	$Revision: 1.12 $	$Date: 2006/08/06 07:13:00 $	MB
#
# Copyright (c) 2004, 2006 Michal Brzozowski
# 
# Permission is hereby granted, free of charge, to any person obtaining
# a copy of this software and associated documentation files (the
# "Software"), to deal in the Software without restriction, including
# without limitation the rights to use, copy, modify, merge, publish,
# distribute, sublicense, and/or sell copies of the Software, and to
# permit persons to whom the Software is furnished to do so, subject to
# the following conditions:
# 
# The above copyright notice and this permission notice shall be included
# in all copies or substantial portions of the Software.
# 
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
# KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
# WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
# NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
# LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
# OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
# WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

import re, sys

#
# Utility functions
#

def cropTo12(value):
    return value & 07777

def cropTo6(value):
    return value & 00077

def cropTo3(value):
    return value & 00007

# convert string to int respecting number base:
# 0x..      - hex
# 0..       - oct
# others    - dec
def strtoint(s):
    if s.startswith('0x'):
        return int(s[2:], 16)
    elif s.startswith('0'):
        return int(s, 8)
    else:
        return int(s, 10)

# translate instruction parameter to _absolute_ address.
# addr is current program counter (starting address of data
# in currently processed source line)
# 
# $+xxx : addr + xxx
# $-xxx : addr - xxx
# id    : labels[id]
# num   : num
def translate(addr, param, labels):
    if param.startswith('$'):
        if len(param) > 2:
            if param[1] == '+':
                return addr + strtoint(param[2:])
            elif param[1] == '-':
                return addr - strtoint(param[2:])
        return addr
    elif param in labels:
        return labels[param]
    else:
        return strtoint(param)

#
# Parameter encoding routines for different addressing modes
# 

# "constant" - value immediately follows opcode
def encodeC(pc, opcode, param1, param2, line):
    return [opcode, cropTo12(param1)]

# "no address" - value is in lowest 6 bits of opcode
def encodeN(pc, opcode, param1, param2, line):
    if param1 < 00000 or param1 > 00077:
        print 'error: value does not fit into extension'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo6(param1)]

# "direct" - address is in lowest 6 bits of opcode
def encodeD(pc, opcode, param1, param2, line):
    if param1 < 00000 or param1 > 00077:
        print 'error: value does not fit into extension'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo6(param1)]

# "memory" - address directly follows opcode
def encodeM(pc, opcode, param1, param2, line):
    return [opcode, cropTo12(param1)]

# "indirect" - address is in lowest 6 bits of opcode
def encodeI(pc, opcode, param1, param2, line):
    if param1 < 00000 or param1 > 00077:
        print 'error: value does not fit into extension'
        print '>>', line
        sys.exit(2)
    elif param1 == 00000:
        print 'error: zero address in indirect mode'
        sys.exit(2)
    else:
        return [opcode + cropTo6(param1)]

# "forward" - offset is in lowest 6 bits
def encodeF(pc, opcode, param1, param2, line):
    v   = cropTo12(param1) - pc
    if v < 00000:
        print 'error: attempt to address backward with forward-relative addressing'
        print '>>', line
        sys.exit(2)
    elif v == 00000:
        print 'error: zero offset in forward-relative addressing'
        print '>>', line
        sys.exit(2)
    elif v > 00077:
        print 'error: too far reference with forward-relative addressing'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo6(v)]

# "backward" - offset is in lowest 6 bits
def encodeB(pc, opcode, param1, param2, line):
    v   = pc - cropTo12(param1)
    if v < 00000:
        print 'error: attempt to address forward with backward-relative addressing'
        print '>>', line
        sys.exit(2)
    elif v == 00000:
        print 'error: zero offset in backward-relative addressing'
        print '>>', line
        sys.exit(2)
    elif v > 00077:
        print 'error: too far reference with backward-relative addressing'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo6(v)]

# address is in range 00050-00057
def encodeSTP(pc, opcode, param1, param2, line):
    if param1 < 00050 or param1 > 00057:
        print 'error: STP can store only in range 0050 to 0057'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo3(param1)]

# address is in range 00060-00067
def encodeSTE(pc, opcode, param1, param2, line):
    if param1 < 00060 or param1 > 00067:
        print 'error: STE can store only in range 0060 to 0067'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo3(param1)]

def encode3(pc, opcode, param1, param2, line):
    if param1 < 00000 or param1 > 00007:
        print 'error: value does not fit in 3 bits'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo3(param1)]

# IO operations - forward-relative param1 and constant param2
def encodeIO(pc, opcode, param1, param2, line):
    return encodeF(pc, opcode, param1, param2, line) + [cropTo12(param2)]

# SLS
def encodeSLS(pc, opcode, param1, param2, line):
    if param1 < 00000 or param1 > 00007:
        print 'error: value does not fit in 3 bits'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + cropTo3(param1)]

# SLJ
def encodeSLJ(pc, opcode, param1, param2, line):
    if param1 < 00000 or param1 > 00007:
        print 'error: value does not fit in 3 bits'
        print '>>', line
        sys.exit(2)
    else:
        return [opcode + (cropTo3(param1) << 3), cropTo12(param2)]

# SJS - not supported
def encodeSJS(pc, opcode, param1, param2, line):
    print 'error: SJS is currently not supported'
    sys.exit(2)

#
# Mnemonics and corresponding codes
#
# map: mnemonic -> (base_opcode, size_in_words, encode_function)
# 

codes   = {
    'ERR':  (00000, 1, None),
    'HLT':  (07700, 1, None),
    'BLS':  (00100, 2, encodeC),
    'PTA':  (00101, 1, None),
    'ATE':  (00105, 2, encodeC),
    'ATX':  (00106, 2, encodeC),
    'ETA':  (00107, 1, None),
    'CTA':  (00130, 1, None),
    'STP':  (00150, 1, encodeSTP),
    'STE':  (00160, 1, encodeSTE),
    'LDN':  (00400, 1, encodeN),
    'LDD':  (02000, 1, encodeD),
    'LDM':  (02100, 2, encodeM),
    'LDI':  (02100, 1, encodeI),
    'LDC':  (02200, 2, encodeC),
    'LDF':  (02200, 1, encodeF),
    'LDS':  (02300, 1, None),
    'LDB':  (02300, 1, encodeB),
    'LCN':  (00500, 1, encodeN),
    'LCD':  (02400, 1, encodeD),
    'LCM':  (02500, 2, encodeM),
    'LCI':  (02500, 1, encodeI),
    'LCC':  (02600, 2, encodeC),
    'LCF':  (02600, 1, encodeF),
    'LCS':  (02700, 1, None),
    'LCB':  (02700, 1, encodeB),
    'STD':  (04000, 1, encodeD),
    'STM':  (04100, 2, encodeM),
    'STI':  (04100, 1, encodeI),
    'STC':  (04200, 2, encodeC),
    'STF':  (04200, 1, encodeF),
    'STS':  (04300, 1, None),
    'STB':  (04300, 1, encodeB),
    'HWI':  (07600, 1, encodeI),
    'MUT':  (00112, 1, None),
    'MUH':  (00113, 1, None),
    'SBN':  (00700, 1, encodeN),
    'SBD':  (03400, 1, encodeD),
    'SBM':  (03500, 2, encodeM),
    'SBI':  (03500, 1, encodeI),
    'SBC':  (03600, 2, encodeC),
    'SBF':  (03600, 1, encodeF),
    'SBS':  (03700, 1, None),
    'SBB':  (03700, 1, encodeB),
    'ADN':  (00600, 1, encodeN),
    'ADD':  (03000, 1, encodeD),
    'ADM':  (03100, 2, encodeM),
    'ADI':  (03100, 1, encodeI),
    'ADC':  (03200, 2, encodeC),
    'ADF':  (03200, 1, encodeF),
    'ADS':  (03300, 1, None),
    'ADB':  (03300, 1, encodeB),
    'RAD':  (05000, 1, encodeD),
    'RAM':  (05100, 2, encodeM),
    'RAI':  (05100, 1, encodeI),
    'RAC':  (05200, 2, encodeC),
    'RAF':  (05200, 1, encodeF),
    'RAS':  (05300, 1, None),
    'RAB':  (05300, 1, encodeB),
    'AOD':  (05400, 1, encodeD),
    'AOM':  (05500, 2, encodeM),
    'AOI':  (05500, 1, encodeI),
    'AOC':  (05600, 2, encodeC),
    'AOF':  (05600, 1, encodeF),
    'AOS':  (05700, 1, None),
    'AOB':  (05700, 1, encodeB),
    'LS1':  (00102, 1, None),
    'LS2':  (00103, 1, None),
    'LS3':  (00110, 1, None),
    'LS6':  (00111, 1, None),
    'RS1':  (00114, 1, None),
    'RS2':  (00115, 1, None),
    'SRD':  (04400, 1, encodeD),
    'SRM':  (04500, 2, encodeM),
    'SRI':  (04500, 1, encodeI),
    'SRC':  (04600, 2, encodeC),
    'SRF':  (04600, 1, encodeF),
    'SRS':  (04700, 1, None),
    'SRB':  (04700, 1, encodeB),
    'LPN':  (00200, 1, encodeN),
    'LPD':  (01000, 1, encodeD),
    'LPM':  (01100, 2, encodeM),
    'LPI':  (01100, 1, encodeI),
    'LPC':  (01200, 2, encodeC),
    'LPF':  (01200, 1, encodeF),
    'LPS':  (01300, 1, None),
    'LPB':  (01300, 1, encodeB),
    'SCN':  (00300, 1, encodeN),
    'SCD':  (01400, 1, encodeD),
    'SCM':  (01500, 2, encodeM),
    'SCI':  (01500, 1, encodeI),
    'SCC':  (01600, 2, encodeC),
    'SCF':  (01600, 1, encodeF),
    'SCS':  (01700, 1, None),
    'SCB':  (01700, 1, encodeB),
    'SRJ':  (00010, 1, encode3),
    'SIC':  (00020, 1, encode3),
    'IRJ':  (00030, 1, encode3),
    'SDC':  (00040, 1, encode3),
    'DRJ':  (00050, 1, encode3),
    'SID':  (00060, 1, encode3),
    'ACJ':  (00070, 1, encode3),
    'SBU':  (00140, 1, encode3),
    'ZJF':  (06000, 1, encodeF),
    'NZF':  (06100, 1, encodeF),
    'PJF':  (06200, 1, encodeF),
    'NJF':  (06300, 1, encodeF),
    'ZJB':  (06400, 1, encodeB),
    'NZB':  (06500, 1, encodeB),
    'PJB':  (06600, 1, encodeB),
    'NJB':  (06700, 1, encodeB),
    'JPI':  (07000, 1, encodeI),
    'JPR':  (07100, 2, encodeC),
    'JFI':  (07100, 1, encodeF),
    'CBC':  (00104, 1, None),
    'CIL':  (00120, 1, None),
    'IBI':  (07200, 2, encodeC),
    'IBO':  (07300, 2, encodeC),
    'INP':  (07200, 2, encodeIO),
    'OUT':  (07300, 2, encodeIO),
    'OTN':  (07400, 1, encodeN),
    'INA':  (07600, 1, None),
    'OTA':  (07677, 1, None),
    'EXC':  (07500, 2, encodeC),
    'EXF':  (07500, 1, encodeF),
    'NOP':  (00001, 1, None),
    'SLS':  (07700, 1, encodeSLS),
    'SLJ':  (07700, 1, encodeSLJ),
    'SJS':  (07700, 1, encodeSJS)
}

#
# Main
#

if len(sys.argv) != 2:
    print 'usage: %s <filename.asm>'
    sys.exit(2)

lines   = []
f       = open(sys.argv[1], 'rU')
for line in f:
    # remove comments
    line    = re.sub(';.*$', '', line).strip()
    if len(line) > 0:
        lines.append(line)

addr    = 00000
start   = 00000
labels  = {}

# pass 1 - prepare labels dictionary
for line in lines:
    words   = line.split()
    
    if len(words) == 3 and words[1] == '=':
        labels[words[0]]    = translate(addr, words[2], labels)
    else:
        if words[0].endswith(':'):
            labels[words[0][:-1]]  = addr
            words               = words[1:]
            
        if len(words) > 0:
            if words[0] == '.ORIGIN':
                addr    = int(words[1], 8)
            elif words[0] == '.START':
                start   = addr
            elif words[0] == '.WORD':
                addr    = addr + len(words[1:])
            else:
                if words[0] in codes:
                    opcode, size, enc   = codes[words[0]]
                    addr                = addr + size
                else:
                    print 'error: unknown mnemonic %s' % words[0]
                    sys.exit(2)


# pass 2 - assemble program
print '*=%04o' % start

addr    = 00000
for line in lines:
    words   = line.split()
    
    if len(words) == 3 and words[1] == '=':
        pass
    else:
        if words[0].endswith(':'):
            words   = words[1:]

        if len(words) > 0:
            if words[0] == '.ORIGIN':
                addr    = strtoint(words[1])
            elif words[0] == '.START':
                pass
            elif words[0] == '.WORD':
                print ';%04o' % addr,
                for w in words[1:]:
                    print '%04o' % translate(addr, w, labels),
                print
                addr    = addr + len(words[1:])
            else:
                param1  = 0
                param2  = 0
                if len(words[1:]) > 0:
                    param1  = translate(addr, words[1], labels)
                if len(words[1:]) > 1:
                    param2  = translate(addr, words[2], labels)
                    
                opcode, size, enc   = codes[words[0]]
                if enc:
                    asm     = enc(addr, opcode, param1, param2, line)
                else:
                    asm     = [opcode]
                print ';%04o' % addr,
                for a in asm:
                    print '%04o' % a,
                print
                addr        = addr + size

