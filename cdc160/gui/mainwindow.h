/*
 * mainwindow.h	$Revision: 1.1 $	$Date: 2006/08/06 07:13:00 $	MB
 *
 * Copyright (c) 2005 Michal Brzozowski
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be included
 * in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
 * KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
 * WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE
 * LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION
 * OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION
 * WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 */

#ifndef __mainwindow_h__
#define __mainwindow_h__

class MainWindow : public wxFrame
{
    public:
        MainWindow();
        virtual ~MainWindow();

    private:
        class Impl;

        Impl    *impl;

        void    onStep(wxCommandEvent &event);
        void    onRun(wxCommandEvent &event);
        void    onStop(wxCommandEvent &event);
        void    onTimer(wxTimerEvent &event);

        DECLARE_EVENT_TABLE()
};

#endif /* __mainwindow_h__ */

